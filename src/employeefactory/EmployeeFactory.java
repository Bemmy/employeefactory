/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeefactory;

public class EmployeeFactory {

    private static EmployeeFactory employeeFactory;
    
    private EmployeeFactory(){}
    
    public static EmployeeFactory getInstance(){
        
        if(employeeFactory == null){
            
            employeeFactory = new EmployeeFactory();
        }
                    return employeeFactory; 
    }
    
    public Employee getEmployee(EmployeeTypes type, double hourlyWage, double hoursWorked, 
            double bonus ){
        
        Employee employee = null;
        
        switch( type){
            case EMPLOYEE: employee = new Employee( hourlyWage , hoursWorked);
            break;
            case MANAGER: employee = new Manager(hourlyWage, hoursWorked, bonus);
            break;
        }    
return employee;
    }
    
}
        