/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package employeefactory;

public class PayRollSimulation {
    
    public static void main(String[] args){
    
    EmployeeFactory employeeFactory = EmployeeFactory.getInstance();
    
    Employee employee1 = employeeFactory.getEmployee(EmployeeTypes.EMPLOYEE, 50.5, 20.25,900);
            
    Employee employee2 = employeeFactory.getEmployee(EmployeeTypes.MANAGER, 50.5, 25.25, 900);
   
    System.out.printf("Employee total wage : $ %.2f\n",employee1.calculatePay());
    System.out.printf("Manager total wage: $ %.2f\n", employee2.calculatePay());
    
    }
}
